import ControladoraA from './ControladoraA.js';
import ControladoraB from './ControladoraB.js';

const emailsTransacoes = ControladoraB.listarEmailsUnicos ();
const idUsuariosTransacoes = emailsTransacoes.map (email => ControladoraA.recuperarIdPeloEmail (email));

console.log (idUsuariosTransacoes);