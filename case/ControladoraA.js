import { leituraArquivo } from './Utils.js';

export default class ControladoraA {
    static recuperarIdPeloEmail (email) {

        let sistemaA = leituraArquivo ('./saidas/output-sistema-a.json');

        if (!sistemaA)
            return null;

        sistemaA = JSON.parse (sistemaA);

        const usuariosEmail = sistemaA.filter (({ ID, Email }) =>
            String (Email).toLowerCase () === String (email).toLowerCase ()
        );

        return usuariosEmail?.length ? usuariosEmail [0].ID : null;

    }
}