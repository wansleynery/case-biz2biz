import { readFileSync } from 'fs';

export function leituraArquivo (caminhoArquivo) {

    const entradaSistemaA = readFileSync (caminhoArquivo,
        {
            encoding: 'utf8',
            flag: 'r'
        });

    return entradaSistemaA;

}