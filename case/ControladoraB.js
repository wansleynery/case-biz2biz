import { leituraArquivo } from './Utils.js';

export default class ControladoraB {
    static listarEmailsUnicos () {

        let sistemaB = leituraArquivo ('./saidas/output-sistema-b.json');

        if (!sistemaB)
            return null;

        sistemaB = JSON.parse (sistemaB);

        const emails = sistemaB.map (transacao => transacao.Email);
        return Array.from (new Set ([ ...emails ]));

    }
}