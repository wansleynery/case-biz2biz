# Sistema A
Realiza a leitura do sistema (via API ou arquivo de saida) para a busca dos dados.

Obs.: Analisar o cacheamento para otimização das chamadas e/ou leitura de streaming do arquivo.


# Sistema B
Realiza a leitura das operações de transação realizadas para filtragem posterior dos usuarios.

Cacheamento pode ser impreciso pois pode conter registros "sujos" ou ainda não finalizados. Analisar!

---

Após a busca dos registros do sistema B contendo as transações, fazemos a listagem dos emails de forma distinta.
Então, armazenamos para, através de iteração, buscar os ids dos usuários do sistema A.